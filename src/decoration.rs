//! Plain text spans with additional decoration.

use std::num::NonZeroU8;

/// Styles that can be applied to decorated spans.
#[derive(PartialEq, Eq, Debug, Clone, Copy, Hash)]
pub enum DecorationStyle {
    Bold,
    Italic,
    Underline,
    /// Horizontally centered.
    Center,
    /// With a horizontal line through it.
    Strikethrough,
    /// Displayed in a monospaced font.
    Monospaced,
    /// Colored with specified sRGB components (as in CSS).
    Color(u8, u8, u8),
    /// Font size at some arbitrary scale.
    Size(NonZeroU8),
    Subscript,
    Superscript,
}

pub fn parse_color(s: &str) -> Option<DecorationStyle> {
    if !s.starts_with('#') {
        // No leading # implies a named color
        return palette::named::from_str(s)
            .map(|rgb| DecorationStyle::Color(rgb.red, rgb.green, rgb.blue));
    } else if !s.is_ascii() {
        // Hex format is never anything but valid ASCII, and enforcing this
        // ensures the slicing we do doesn't split any codepoints (and panic).
        // Imagine a value string that contains a single three-byte codepoint.
        return None;
    }

    // Hex RGB; #rgb or #rrggbb
    fn parse_part(part: &str) -> Option<u8> {
        u8::from_str_radix(part, 16).ok()
    }

    let r;
    let g;
    let b;
    if s.len() == 4 {
        fn expand(x: u8) -> u8 {
            x | x << 4
        }
        r = parse_part(&s[1..2]).map(expand);
        g = parse_part(&s[2..3]).map(expand);
        b = parse_part(&s[3..]).map(expand);
    } else if s.len() == 7 {
        r = parse_part(&s[1..3]);
        g = parse_part(&s[3..5]);
        b = parse_part(&s[5..]);
    } else {
        return None;
    }

    match (r, g, b) {
        (Some(r), Some(g), Some(b)) => Some(DecorationStyle::Color(r, g, b)),
        _ => None,
    }
}

pub fn parse_size(s: &str) -> Option<DecorationStyle> {
    match u8::from_str_radix(s, 10) {
        Ok(x) if x < 1 || x > 48 => None,
        Ok(x) => Some(DecorationStyle::Size(NonZeroU8::new(x).unwrap())),
        Err(_) => return None,
    }
}

#[cfg(test)]
mod tests {
    use crate::decoration::parse_size;
    use crate::*;
    use crate::{DecorationStyle::*, Point::*, Span::*};
    use std::num::NonZeroU8;

    #[test]
    fn bold_text() {
        assert_eq!(
            collect_parse("[b]BOLD![/b]"),
            vec![Start(Decorated(Bold)), Text("BOLD!"), End(Decorated(Bold)),]
        )
    }

    #[test]
    fn underlined_text() {
        assert_eq!(
            collect_parse("[u]um[/u]"),
            vec![
                Start(Decorated(Underline)),
                Text("um"),
                End(Decorated(Underline)),
            ]
        )
    }

    #[test]
    fn accepts_colors() {
        assert_eq!(
            collect_parse("[color=red]asdf[/color]"),
            vec![
                Start(Decorated(Color(0xff, 0, 0))),
                Text("asdf"),
                End(Decorated(Color(0xff, 0, 0))),
            ]
        );

        assert_eq!(
            collect_parse("[color=#81f][/color]"),
            vec![
                Start(Decorated(Color(0x88, 0x11, 0xff))),
                End(Decorated(Color(0x88, 0x11, 0xff))),
            ]
        );

        assert_eq!(
            collect_parse("[color=#01FE9A]and[/color]"),
            vec![
                Start(Decorated(Color(1, 0xfe, 0x9a))),
                Text("and"),
                End(Decorated(Color(1, 0xfe, 0x9a))),
            ]
        );
    }

    #[test]
    fn rejects_invalid_colors() {
        assert_eq!(super::parse_color("beyblade"), None);
        assert_eq!(super::parse_color("#\u{1000}"), None);
        assert_eq!(super::parse_color("#blue"), None);
        assert_eq!(super::parse_color("#0g0000"), None);
    }

    #[test]
    fn size() {
        let ten = NonZeroU8::new(10).unwrap();
        assert_eq!(
            collect_parse("[size=10]midsize[/size]"),
            vec![
                Start(Decorated(Size(ten))),
                Text("midsize"),
                End(Decorated(Size(ten))),
            ]
        );
        // Size must be in range 1..=48
        assert_eq!(parse_size("0"), None);
        assert_eq!(parse_size("50"), None);
        // Sizes are base 10
        assert_eq!(parse_size("0xa"), None);
    }

    #[test]
    fn center_works() {
        assert_eq!(
            collect_parse("[center][/center]"),
            vec![Start(Decorated(Center)), End(Decorated(Center)),]
        );
    }

    #[test]
    fn strike() {
        assert_eq!(
            collect_parse("[strike]Moo[/strike]"),
            vec![
                Start(Decorated(Strikethrough)),
                Text("Moo"),
                End(Decorated(Strikethrough)),
            ]
        );
    }

    #[test]
    fn mono() {
        assert_eq!(
            collect_parse("[mono]BEEP[/mono]"),
            vec![
                Start(Decorated(Monospaced)),
                Text("BEEP"),
                End(Decorated(Monospaced)),
            ]
        );
    }

    #[test]
    fn sub_superscript() {
        assert_eq!(
            collect_parse("H[sub]2[/sub]O"),
            vec![
                Text("H"),
                Start(Decorated(Subscript)),
                Text("2"),
                End(Decorated(Subscript)),
                Text("O"),
            ]
        );

        assert_eq!(
            collect_parse("x[sup]3[/sup]"),
            vec![
                Text("x"),
                Start(Decorated(Superscript)),
                Text("3"),
                End(Decorated(Superscript)),
            ]
        );
    }
}
