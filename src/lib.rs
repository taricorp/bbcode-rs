//! A parsing library for [bbcode](https://en.wikipedia.org/wiki/BBCode)

use cached::cached_key;
use lazy_static::lazy_static;
use regex::{Regex, RegexBuilder};
use smallstr::SmallString;
use std::fmt::{Debug, Write};

mod decoration;
mod list;

pub use decoration::DecorationStyle;
pub use list::ListStyle;
use std::convert::Infallible;

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Span<'a> {
    /// Simple decorations, such as text color or font weight.
    Decorated(DecorationStyle),
    /// A blockquote with optional attribution.
    Quote(Option<&'a str>),
    /// A block of code, displayed verbatim.
    Code,
    /// A list of items with a specified style.
    List(ListStyle),
    /// An item in a list.
    ListItem(ListStyle),
    /// A hyperlink to the provided URL, or the body text if omitted.
    Link(Option<&'a str>),
    /// A picture, displayed inline from the URL in the body text.
    ///
    /// Because the contents are the URL, this span never contains
    /// other spans.
    Image,
    /// An embedded video from Youtube.
    ///
    /// Like an image, the URL is inline in the body text and it never
    /// contains other spans.
    YoutubeEmbed,
    // A horizontal line, like html <hr>
    HorizontalRule,
    // TODO extra items
    // [h1] - [h6]
}

impl<'a> Span<'a> {
    fn is_self_closing(&self) -> bool {
        match self {
            Self::HorizontalRule => true,
            _ => false
        }
    }

    /// Return true if a span of this type contains only text.
    fn is_verbatim(&self) -> bool {
        match self {
            Self::Code | Self::Image | Self::YoutubeEmbed => true,
            _ => false,
        }
    }

    fn to_tag(&self) -> &'static str {
        use DecorationStyle::*;
        use Span::*;

        match self {
            Decorated(Bold) => "b",
            Decorated(Italic) => "i",
            Decorated(Underline) => "u",
            Decorated(Center) => "center",
            Decorated(Strikethrough) => "strike",
            Decorated(Monospaced) => "mono",
            Decorated(Color(..)) => "color",
            Decorated(Size(_)) => "size",
            Decorated(Subscript) => "sub",
            Decorated(Superscript) => "sup",
            Quote(_) => "quote",
            Code => "code",
            List(_) => "list",
            // List items are treated specially so this string isn't actually
            // relevant, but returning a useless tag here is significantly
            // simpler than making this function return an Option and ignoring
            // list items; instead this tag will never match because the
            // tag patterns don't admit this string.
            ListItem(_) => "*",
            Link(_) => "url",
            Image => "img",
            YoutubeEmbed => "youtube",
            HorizontalRule => unreachable!("hr is self-closing"),
        }
    }

    fn from_tag(name: &'a str, value: Option<&'a str>) -> Option<Self> {
        let mut lowercase = SmallString::<[u8; 16]>::from_str(name);
        lowercase.make_ascii_lowercase();

        use DecorationStyle::*;
        use Span::*;

        let kind = match (lowercase.as_str(), value) {
            ("b", None) => Decorated(Bold),
            ("i", None) => Decorated(Italic),
            ("u", None) => Decorated(Underline),
            ("center", None) => Decorated(Center),
            ("strike", None) => Decorated(Strikethrough),
            ("mono", None) => Decorated(Monospaced),
            ("color", Some(s)) => return decoration::parse_color(s).map(Decorated),
            ("size", Some(s)) => return decoration::parse_size(s).map(Decorated),
            ("sub", None) => Decorated(Subscript),
            ("sup", None) => Decorated(Superscript),
            ("quote", opt) => Quote(opt),
            ("code", None) => Code,
            ("list", None) => List(ListStyle::Unordered),
            ("list", Some(s)) => List(if s.chars().all(char::is_alphabetic) {
                ListStyle::Alphabetic
            } else if s.chars().all(char::is_numeric) {
                ListStyle::Numeric
            } else {
                return None;
            }),
            ("url", opt) => Link(opt),
            ("img", None) => Image,
            ("youtube", None) => YoutubeEmbed,
            ("hr", None) => HorizontalRule,
            _ => return None,
        };
        Some(kind)
    }
}

pub trait SegmentHandler<'a> {
    type Err;

    fn begin_span(&mut self, span: &Span<'a>) -> Result<(), Self::Err>;
    fn text(&mut self, s: &'a str) -> Result<(), Self::Err>;
    fn end_span(&mut self, span: &Span<'a>) -> Result<(), Self::Err>;
}

lazy_static! {
    static ref TAG_REGEX: Regex = Regex::new(
        r#"(?ix)
        \[ (?:
            (?: # An open tag
                (?P<name_open>[0-9[:alpha:]]+)
                # Optional name=value, which may be quoted
                (?: = (?:
                    (?:"(?P<value_quot>[^"]+)")
                    |
                    (?:'(?P<value_apos>[^']+)')
                    |
                    (?P<value>[^\]]+)
                ))?
            ) | (?: # A close tag
                /(?P<name_close>[0-9[:alpha:]]+)
            ) | # A list item
            (?P<list_item>\*)
        ) \]
        "#,
    )
    .expect("Unable to compile tag regex");
}

cached_key! {
    TAG_CLOSE_RE: cached::UnboundCache<&'static str, Regex> = cached::UnboundCache::new();
    Key = span.to_tag();
    fn tag_close_re(span: &Span) -> Regex = {
        let tag = span.to_tag();
        let mut pattern = String::with_capacity(tag.len() + 5);
        let _ = write!(&mut pattern, r"\[/{}\]", tag);

        RegexBuilder::new(&pattern)
            .case_insensitive(true)
            .build()
            .expect("Close tag RE became invalid")
    }
}

/// Walk over segments in the input, emitting events to the provided handlers.
///
/// This works like an XML SAX parser, where [SegmentHandler::begin_span] is called for each open
/// tag, and [SegmentHandler::end_span] for each end tag. Plain text is emitted through
/// [SegmentHandler::text] between and inside spans.
///
/// Unclosed tags are implicitly closed when a parent is closed or the end of input is reached.
/// While this differs somewhat from traditional bbcode parsing that ignores unclosed tags
/// (treating them as plain text), the traditional approach is extremely costly because it involves
/// a lot of backtracking to determine where tags close before handling them, and tends to be
/// incapable of correctly handling out-of-order closing tags.
pub fn walk_segments<'a, SF: SegmentHandler<'a>>(
    input: &'a str,
    mut segment_f: SF,
) -> Result<(), SF::Err> {
    let mut open_spans = Vec::<Span>::new();

    // Two offsets into the input: input_ofs is the start of a region that may be text,
    // while search_pos is the point at which we're looking for tags.
    //
    // search_pos may advance past input_ofs when we find something that looks like
    // a tag but isn't a valid one, in which case we advance it to keep looking for
    // tags. This allows us to emit text all at once instead of splitting it when we
    // find something that could be a tag but isn't a valid one.
    let mut input_ofs = 0usize;
    let mut search_pos = input_ofs;

    while search_pos < input.len() {
        // search may move past input, but the opposite is an error
        debug_assert!(search_pos >= input_ofs);

        // If the last opened tag is verbatim it doesn't permit nested tags, so
        // only search for a matching close tag.
        if open_spans.last().map(Span::is_verbatim).unwrap_or(false) {
            // Case-insensitive search for [/tag] only; it's surprisingly hard
            // to do in general, so delegate to regex.
            let span = open_spans.last().unwrap();
            let close_re = tag_close_re(span);

            let close_match = match close_re.find(&input[search_pos..]) {
                Some(m) => m,
                // No match: fall off the end of input
                None => break,
            };
            // Found a close tag for the verbatim span; emit text up to it, close
            // the span and restart from past the close tag.
            segment_f.text(&input[input_ofs..search_pos + close_match.start()])?;
            segment_f.end_span(span)?;
            open_spans.pop();
            search_pos += close_match.end();
            input_ofs = search_pos;
            continue;
        }

        // Look for any opening or closing tag
        let capture = match TAG_REGEX.captures(&input[search_pos..]) {
            Some(c) => c,
            // No tags here, it's all plain text. Stop searching for tags.
            None => break,
        };
        // We've found something that looks like either an open or close tag,
        // but it may not be valid. Inspect each case in more detail.

        let full_capture = capture.get(0).unwrap();
        // Capture positions are offsets from search_pos due to slicing the input;
        // shift them to be relative to the whole input.
        let tag_start = search_pos + full_capture.start();
        let tag_end = search_pos + full_capture.end();

        if let Some(name) = capture.name("name_close") {
            let name = name.as_str();
            // Found a closing tag. See if it matches any span that is currently open
            // (even one that's not the most recent), getting the index of it in open_spans.
            let matched_span: Option<usize> =
                open_spans.iter().enumerate().rev().find_map(|(i, span)| {
                    // Case-insensitive comparison of non-ASCII is much harder,
                    // but bbcode is generally ASCII-only anyway.
                    debug_assert!(span.to_tag().is_ascii());
                    if span.to_tag().eq_ignore_ascii_case(name) {
                        Some(i)
                    } else {
                        None
                    }
                });

            let span_idx: usize = match matched_span {
                None => {
                    // There is no open span matching this tag; treat it as plain text
                    // and continue searching for tags.
                    search_pos = tag_end;
                    continue;
                }
                Some(i) => i,
            };

            // Emit text contained in the current span
            emit_text(&mut segment_f, &input[input_ofs..tag_start])?;
            // Close the matched span, and any unclosed spans; most recent first
            for span in open_spans.drain(span_idx..).rev() {
                segment_f.end_span(&span)?;
            }
            debug_assert_eq!(open_spans.len(), span_idx);

            // Consume the tag from the input
            input_ofs = tag_end;
            search_pos = tag_end;
        } else if let Some(name) = capture.name("name_open") {
            // Capture looks like an open tag. Try to convert it into a span.
            let value = capture
                .name("value")
                .or(capture.name("value_quot"))
                .or(capture.name("value_apos"))
                .map(|m| m.as_str());
            let span = match Span::from_tag(name.as_str(), value) {
                None => {
                    // Not actually a valid open tag; treat as text and continue
                    // searching for tags after this.
                    search_pos = tag_end;
                    continue;
                }
                Some(s) => s,
            };

            // This is a valid open tag; emit text up to it and start a new span.
            emit_text(&mut segment_f, &input[input_ofs..tag_start])?;
            segment_f.begin_span(&span)?;

            // Self-closing tags close immediately, otherwise put it on the context stack
            if span.is_self_closing() {
                segment_f.end_span(&span)?;
            } else {
                open_spans.push(span);
            }

            // Consume input through the match
            input_ofs = tag_end;
            search_pos = tag_end;
        } else if capture.name("list_item").is_some() {
            // List items are very strange: they are never closed explicitly
            // (only by the appearance of another list item or the parent
            // list closing) and they only appear as immediate children
            // of [list] tags.
            // This is sufficiently weird that they're special-cased here.
            match open_spans.last() {
                Some(Span::List(style)) => {
                    // First marked item in this list. There may be (arguably malformed)
                    // text between the list start and this first item, so emit that
                    // first.
                    emit_text(&mut segment_f, &input[input_ofs..tag_start])?;
                    let span = Span::ListItem(*style);
                    segment_f.begin_span(&span)?;
                    open_spans.push(span);
                }
                Some(span @ Span::ListItem(_)) => {
                    // Already inside a list item; generate end and start
                    // events to close it and open a new identical one.
                    debug_assert!(
                        match open_spans[open_spans.len() - 2] {
                            Span::List(_) => true,
                            _ => false,
                        },
                        "ListItem spans must be inside a List"
                    );
                    // This also acts as the close tag for the previous item,
                    // so emit its text first.
                    emit_text(&mut segment_f, &input[input_ofs..tag_start])?;
                    segment_f.end_span(span)?;
                    segment_f.begin_span(span)?;
                }
                Some(_) | None => {
                    // Not valid in this context; it's plain text
                    search_pos = tag_end;
                    continue;
                }
            }

            // Advance past the list item marker
            input_ofs = tag_end;
            search_pos = tag_end;
        } else {
            unreachable!("Tag regex must match open, close, or list item");
        }
    }

    // Exhausted the input. Emit any trailing text.
    emit_text(&mut segment_f, &input[input_ofs..])?;

    // Close any spans that were left open, most recent first.
    for span in open_spans.iter().rev() {
        segment_f.end_span(&span)?;
    }

    Ok(())
}

fn emit_text<'a, SF: SegmentHandler<'a>>(handler: &mut SF, text: &'a str) -> Result<(), SF::Err> {
    if !text.is_empty() {
        handler.text(text)
    } else {
        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Point<'a> {
    Start(Span<'a>),
    Text(&'a str),
    End(Span<'a>),
}

struct Collect<'a>(Vec<Point<'a>>);

impl<'a> SegmentHandler<'a> for &mut Collect<'a> {
    type Err = Infallible;

    fn begin_span(&mut self, span: &Span<'a>) -> Result<(), Self::Err> {
        self.0.push(Point::Start(span.clone()));
        Ok(())
    }

    fn text(&mut self, s: &'a str) -> Result<(), Self::Err> {
        self.0.push(Point::Text(s));
        Ok(())
    }

    fn end_span(&mut self, span: &Span<'a>) -> Result<(), Self::Err> {
        self.0.push(Point::End(span.clone()));
        Ok(())
    }
}

/// Parse the given text, returning every emitted element.
pub fn collect_parse(s: &str) -> Vec<Point> {
    let mut collect = Collect(vec![]);
    walk_segments(s, &mut collect).unwrap();
    let points = collect.0;

    // Simple check: there are equal numbers of span start and endpoints
    let net_depth: isize = points
        .iter()
        .filter_map(|p| match p {
            Point::Start(_) => Some(1),
            Point::End(_) => Some(-1),
            _ => None,
        })
        .sum();
    debug_assert_eq!(
        net_depth, 0,
        "Equal numbers of span start and ends should be emitted: {:#?}",
        points
    );

    points
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::{DecorationStyle::*, ListStyle::*, Point::*, Span::*};

    #[test]
    fn nested_segments() {
        let segments = collect_parse("[b]Foo[i]bar[/i][/b]");
        assert_eq!(
            segments,
            vec![
                Start(Decorated(Bold)),
                Text("Foo"),
                Start(Decorated(Italic)),
                Text("bar"),
                End(Decorated(Italic)),
                End(Decorated(Bold))
            ]
        )
    }

    #[test]
    fn close_unclosed_spans() {
        // At end of input
        assert_eq!(
            collect_parse("[u]wut"),
            vec![
                Start(Decorated(Underline)),
                Text("wut"),
                End(Decorated(Underline))
            ]
        );

        // When closing parent
        assert_eq!(
            collect_parse("[b][u][i]TEST[/b]"),
            vec![
                Start(Decorated(Bold)),
                Start(Decorated(Underline)),
                Start(Decorated(Italic)),
                Text("TEST"),
                End(Decorated(Italic)),
                End(Decorated(Underline)),
                End(Decorated(Bold)),
            ]
        );
    }

    #[test]
    fn parse_image() {
        assert_eq!(
            collect_parse("[img]http://example.com/foo.webp[/img]"),
            vec![
                Start(Image),
                Text("http://example.com/foo.webp"),
                End(Image)
            ]
        )
    }

    #[test]
    fn parse_quote() {
        assert_eq!(
            collect_parse(
                "[quote]No[/quote]\n\
                           [quote='Gandhi']Yes[/quote]"
            ),
            vec![
                Start(Quote(None)),
                Text("No"),
                End(Quote(None)),
                Text("\n"),
                Start(Quote(Some("Gandhi"))),
                Text("Yes"),
                End(Quote(Some("Gandhi"))),
            ]
        );
    }

    #[test]
    fn parse_url() {
        assert_eq!(
            collect_parse("[url]http://example.com/[/url]"),
            vec![
                Start(Link(None)),
                Text("http://example.com/"),
                End(Link(None)),
            ]
        );

        assert_eq!(
            collect_parse("[url='http://example.com/']Example web site[/url]"),
            vec![
                Start(Link(Some("http://example.com/"))),
                Text("Example web site"),
                End(Link(Some("http://example.com/"))),
            ]
        );
    }

    #[test]
    fn parse_list() {
        // Default style
        assert_eq!(
            collect_parse("[list][*] One [*] Two[/list]"),
            vec![
                Start(List(Unordered)),
                Start(ListItem(Unordered)),
                Text(" One "),
                End(ListItem(Unordered)),
                Start(ListItem(Unordered)),
                Text(" Two"),
                End(ListItem(Unordered)),
                End(List(Unordered)),
            ]
        );
        // Users often fail to include item markers, making lists pointless.
        // It shouldn't fail horribly, though.
        assert_eq!(
            collect_parse(
                "[list=1]Common misuse\n\
                       Second element[/list]"
            ),
            vec![
                Start(List(Numeric)),
                Text("Common misuse\nSecond element"),
                End(List(Numeric)),
            ]
        );
        // Alphabetic style is accepted
        assert_eq!(
            collect_parse("[list='abc'][*] A[/list]"),
            vec![
                Start(List(Alphabetic)),
                Start(ListItem(Alphabetic)),
                Text(" A"),
                End(ListItem(Alphabetic)),
                End(List(Alphabetic)),
            ]
        );
        // Invalid style is not a valid list at all
        assert_eq!(collect_parse("[list=q3]"), vec![Text("[list=q3]")]);
        // List items not in lists are not list items
        assert_eq!(
            collect_parse("[*] Stars! [*]"),
            vec![Text("[*] Stars! [*]")]
        );
    }

    #[test]
    fn weird_unicode_no_panic() {
        // Fuzzing found these particular sequences caused bad string slicing;
        // they're not valid, but shouldn't panic.
        collect_parse("[u[][Ȧ][Ȧ");
        collect_parse("[color=#\u{1000}]");
    }

    #[test]
    fn value_requires_equals() {
        // Tag values must be separated by '=' (this was initially a bug in
        // the tag regex).
        assert_eq!(collect_parse("[color#aaa]"), vec![Text("[color#aaa]")])
    }

    #[test]
    fn invalid_tag_is_text() {
        // Unrecognized tags are treated as plain text.
        assert_eq!(
            collect_parse("[b]Hello [underline]?[/underline][/b]"),
            vec![
                Start(Decorated(Bold)),
                Text("Hello [underline]?[/underline]"),
                End(Decorated(Bold)),
            ]
        )
    }

    #[test]
    fn tags_ignore_case() {
        // The casing of open and close tags is ignored.
        assert_eq!(
            collect_parse("[IMG]or not[/iMg]"),
            vec![Start(Image), Text("or not"), End(Image),]
        );
    }

    #[test]
    fn code_ignores_internal_tags() {
        // [code] is a verbatim tag and doesn't admit other tags inside it.
        assert_eq!(
            collect_parse("[code]Put bbcode in [code] blocks to show it [i]verbatim[/i][/code]"),
            vec![
                Start(Code),
                Text("Put bbcode in [code] blocks to show it [i]verbatim[/i]"),
                End(Code),
            ]
        );

        // Slightly different code path from non-verbatim tags, verify
        // unclosed tag gets closed.
        assert_eq!(
            collect_parse("[code]Unterminated!"),
            vec![Start(Code), Text("Unterminated!"), End(Code),]
        );
    }

    #[test]
    fn youtube_embed() {
        assert_eq!(
            collect_parse("[youtube]https://www.youtube.com/watch?v=apeZO6C0ZeA[/youtube]"),
            vec![
                Start(YoutubeEmbed),
                Text("https://www.youtube.com/watch?v=apeZO6C0ZeA"),
                End(YoutubeEmbed)
            ]
        );

        assert_eq!(
            collect_parse("[youtube][b]Not bold[/b][/youtube]"),
            vec![
                Start(YoutubeEmbed),
                Text("[b]Not bold[/b]"),
                End(YoutubeEmbed)
            ]
        );
    }

    #[test]
    fn horizontal_rule() {
        assert_eq!(
            collect_parse("Hi[hr]Bye"),
            vec![
                Text("Hi"),
                Start(HorizontalRule),
                End(HorizontalRule),
                Text("Bye"),
            ]
        )
    }
}
