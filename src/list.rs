//! Lists of items.

/// The general appearance of a list.
#[derive(PartialEq, Eq, Debug, Clone, Copy, Hash)]
pub enum ListStyle {
    /// No particular order; items usually marked with bullet points.
    ///
    /// Like CSS `list-style-type` `disc`.
    Unordered,
    /// Items numbered in increasing order.
    ///
    /// Like CSS `list-style-type` `decimal`.
    Numeric,
    /// Items marked with latin letters.
    ///
    /// Like CSS `list-style-type` `lower-alpha`.
    Alphabetic,
}
