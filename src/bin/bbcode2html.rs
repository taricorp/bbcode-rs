extern crate bbcode;

use std::convert::Infallible;
use std::io::Read;

use bbcode::Span;

struct Handler;

impl<'a> bbcode::SegmentHandler<'a> for &mut Handler {
    type Err = Infallible;

    fn begin_span(&mut self, span: &Span) -> Result<(), Self::Err> {
        println!("Begin: {:?}", span);
        Ok(())
    }

    fn text(&mut self, s: &str) -> Result<(), Self::Err> {
        println!("Text: {:?}", s);
        Ok(())
    }

    fn end_span(&mut self, span: &Span) -> Result<(), Self::Err> {
        println!("End: {:?}", span);
        Ok(())
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let text = {
        let stdin = std::io::stdin();
        let mut l = stdin.lock();
        let mut s = String::new();
        l.read_to_string(&mut s)?;
        s
    };

    bbcode::walk_segments(&text, &mut Handler)?;

    Ok(())
}
